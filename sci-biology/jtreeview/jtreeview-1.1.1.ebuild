# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $
inherit java-pkg-2 java-ant-2
DESCRIPTION="Java TreeView renders gene expression data into several interactive
views."
MY_P="TreeView"
HOMEPAGE="http://jtreeview.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}-${PV}-src.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""


DEPEND="dev-java/jarbundler 
		dev-java/xml-commons-external 
		dev-java/fop 
		dev-java/xerces
		dev-java/nanoxml
		dev-java/jarbundler"
RDEPEND=">=virtual/jre-1.4
		dev-java/nanoxml"

S=${WORKDIR}/${MY_P}-${PV}-src
src_unpack() {
	unpack ${A}

	cd ${S}/compile_lib
	java-pkg_jar-from --build-only xerces-2 xercesImpl.jar
	java-pkg_jar-from --build-only xml-commons-external-1.3 xml-apis.jar
	java-pkg_jar-from --build-only jarbundler jarbundler.jar jarbundler-1.8.1.jar
	cd ${S}/lib
	java-pkg_jar-from nanoxml nanoxml.jar nanoxml-2.2.2.jar

}
