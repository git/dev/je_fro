# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

ESVN_REPO_URI="svn://svn.handbrake.fr/HandBrake/trunk@4365"
# REV 4365 worked for me a long time.

inherit subversion gnome2-utils

DESCRIPTION="Open-source DVD to MPEG-4 converter."
HOMEPAGE="http://handbrake.fr/"
LICENSE="GPL-2"
SLOT="0"

KEYWORDS=""

IUSE=""
RDEPEND=""
DEPEND="sys-libs/zlib
	dev-lang/yasm
	=net-libs/webkit-gtk-1.2.7
	dev-libs/fribidi
	>=dev-lang/python-2.4.6
	|| ( >=net-misc/wget-1.11.4 >=net-misc/curl-7.19.4 )
	$RDEPEND"

src_configure() {

	local myconf=""

	./configure --force --prefix=/usr/local ${myconf} || die "configure failed"

}

src_compile() {

	cd "${S}/build" || die "cannot find build dir"

	WANT_AUTOMAKE=1.11 make -j6 build|| die "failed compiling ${PN}"

}

src_install() {
# pwd
echo "${S}"
	cd "${S}/build" || die "cannot find build dir"

	make DESTDIR="${D}" install || die "failed installing ${PN}"

}

pkg_preinst() {

	gnome2_icon_savelist

}

pkg_postinst() {

	gnome2_icon_cache_update

}

pkg_postrm() {

	gnome2_icon_cache_update

}
